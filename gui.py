# -*- coding: utf-8 -*-

'''
ZetCode Advanced PyQt5 tutorial 

This program animates the size of a
widget with QPropertyAnimation.

Author: Jan Bodnar
Website: zetcode.com 
Last edited: August 2017
'''

from PyQt5.QtWidgets import * #QWidget, QApplication, QFrame, QPushButton, QLabel
from PyQt5.QtCore import * #QRect, QPropertyAnimation
from PyQt5.QtGui import * #QPixmap
import sys
             
             
class Example(QWidget):

    def __init__(self):
        super().__init__()
        self.setFixedSize(480, 640)
        
        self.initUI()
        
    def initUI(self):
        # Halp Logo top left
        self.pic = QLabel(self)
        self.pic.setPixmap(QPixmap("halp.png").scaled(72, 72, Qt.KeepAspectRatio))
        self.pic.move(0,0)

        # Main title for gui
        self.title = QLabel(self)
        self.title.setText("Guardian | Halp Edition")
        self.title.move(70,22)
        self.title.setFont(QFont("Cali", 30, QFont.Bold))
        self.title.setStyleSheet('color: #bfbfbf')
        
        self.setWindowTitle('Guardian | HALP Version')
        self.setStyleSheet("background-color: #2e2e2e;")
        self.show()

if __name__ == "__main__":
    
    app = QApplication([])
    ex = Example()
    ex.show()
    app.exec_()